---
layout: home
title: "Bienvenue !"
menu: menu
menutitle: Accueil
weight: 1
---

[Moi][moi], c'est Robin. [J'écris][blog] ce qui me passe par la tête sur mon blog, et entre deux [je crée][portfolio] des sites web. Si vous voulez, vous pouvez aussi [me contacter][contact].

Bonne visite !

[moi]: {{ site.baseurl }}{% link _pages/moi.md %}
[blog]: {{ site.baseurl }}{% link _pages/blog.md %}
[portfolio]: {{ site.baseurl }}{% link _pages/portfolio.md %}
[contact]: {{ site.baseurl }}{% link _pages/contact.md %}
