---
layout: page
title: Moi
permalink: /moi/
menu: menu
weight: 20
---

Je m'appelle Robin, j'ai 23 ans (sauf quand j'oublie de mettre mon âge à jour).

Ça fait [presque dix ans][debuts-webmaster] que je programme des sites web, et récemment je me suis mis aux générateurs de sites statiques : j'utilise [Jekyll][jekyll].

Depuis quelques mois j'habite à Chiang Mai, où j'apprends à parler et cuisiner thaï, et à survivre dans un climat tropical.

La plupart du temps, vous me trouverez dans un café avec [Clara][clara], à créer un site web ou écrire un article sur mon ordi portable.

J'aime :
* le café filtre, j'en bois au moins 3 par jour
* les logiciels libres
* voyager
* et d'autres choses aussi, mais là il est tard et je vais aller me coucher

[debuts-webmaster]: {{ site.baseurl }}{% post_url /2018-07-30-mes-debuts-de-webmaster %}
[jekyll]: https://jekyllrb.com
[clara]: https://clarale.com
