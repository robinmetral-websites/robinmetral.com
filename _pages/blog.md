---
layout: blog
title: Blog
permalink: /blog/
menu: menu
weight: 10
---

En ce moment il reste assez technique, mais bientôt mon blog parlera de tout et de rien. [Si vous me connaissez][moi], vous savez à quels sujets d'articles vous attendre (le café).

[moi]: {{ site.baseurl }}{% link _pages/moi.md %}
