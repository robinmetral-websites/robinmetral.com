---
layout: page
title: Liens
permalink: /liens/
menu: menu
weight: 40
---

Ceci est ma page liens.

[Écrivez-moi][contact] pour me suggérer un lien (sauf celui de votre propre site) ou pour me signaler un éventuel lien mort !

# Blogs

- [Wait But Why][wait-but-why] (en anglais) : des longs articles sur les nouvelles technologies, le futur, le présent et bien d'autres choses encore
- [Clara][clara] : typographie, design

# Ressources pour développeurs web

- [freeCodeCamp][freecodecamp] (en anglais) : une plateforme open-source et gratuite pour apprendre à programmer
- [The Front-End Checklist][frontend-checklist] (en anglais) : une checklist pour développeurs méticuleux (et le projet sur [Github][frontend-checklist-github])
- [Web Typography][web-typography] (en anglais) : des ressources sur la typographie sur le web

[contact]: {{ site.baseurl }}{% link _pages/contact.md %}
[clara]: https://clarale.com
[wait-but-why]: https://waitbutwhy.com
[freecodecamp]: https://www.freecodecamp.org/
[frontend-checklist]: https://frontendchecklist.io
[frontend-checklist-github]: https://github.com/thedaviddias/Front-End-Checklist
[web-typography]: https://deanhume.github.io/typography/
