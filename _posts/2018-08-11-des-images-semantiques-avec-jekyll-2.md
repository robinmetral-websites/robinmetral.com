---
layout: post
title: "Des images sémantiques avec Jekyll, option 2"
date: 2018-08-11 +0700
contenttype: perso
categories: images, css, jekyll, tutos, accessibilite, semantique, plugin
excerpt: "Une meilleure option pour des images sémantiques avec le plugin jekyll-figure."
---
Il y a quelques jours, j'ai partagé une [solution <abbr title="Do It Yourself">DIY</abbr> pour des images sémantiquement correctes avec Jekyll][images-semantiques-jekyll].

**Aujourd'hui je veux vous parler d'une option alternative : le plugin [`jekyll-figure`][jekyll-figure]**.

# Installer jekyll-figure

`jekyll-figure`, comme beaucoup de plugins, est une *Ruby gem*.

C'est pratique, parce qu'installer des gems et les garder à jour est super facile !

Il suffit d'ajouter une ligne au `Gemfile` à la racine de notre dossier Jekyll :

```ruby
group :jekyll_plugins do # Tous nos plugins
    gem "jekyll-feed", "~> 0.9" # Intégré à Jekyll par défaut
    gem "jekyll-seo-tag", "~> 2.1" # Intégré à Jekyll par défaut
    gem "jekyll-figure" # On ajoute une nouvelle ligne pour chaque nouveau plugin
end
```

Puis une commande toute simple (dans votre dossier Jekyll) installera tous les nouveaux plugins : 

```
bundle install
```

Pour vérifier et installer des mises à jour de gems, y compris de jekyll lui-même, c'est comme ça :

```
bundle update
```


# Utiliser jekyll-figure

Pour générer une image avec légende en HTML sémantiquement correct avec `jekyll-figure`, rien de plus simple.

Voici l'utilisation du plugin en Markdown :

```markdown
{% raw %}
{% figure caption:"Le logo de **Jekyll** et son clin d'oeil à Robert Louis Stevenson" %}
![Le logo de Jekyll](/assets/images/2018-08-07-jekyll-logo.png)
{% endfigure %}
{% endraw %}
```

Comme vous le voyez, il suffit d'entourer notre image Markdown des tags Liquid {% raw %}`{% figure %}` et `{% endfigure %}`{% endraw %} en y précisant notre légende avec `caption:"Ma légende"`.

Et, cerise sur le gâteau, on peut aussi utiliser Markdown dans la légende ! Ici, je voulais "Jekyll" en gras.

Résultat :

{% figure caption:"Le logo de **Jekyll** et son clin d'oeil à Robert Louis Stevenson" %}
![Le logo de Jekyll](/assets/images/2018-08-07-jekyll-logo.png)
{% endfigure %}

*Pour plus d'infos sur l'installation et l'utilisation de `jekyll-figure`, rendez-vous sur [sa repo Github][jekyll-figure] !*

**Alors, quelle option utiliser ? Le plugin ou la solution DIY ?**

# Plugins ou DIY ?

Avec Wordpress, la question ne se pose même pas.

C'est un système complexe, et les plugins font partie à part entière du processus de développement web. On ne peut pas faire sans !

> Besoin d'un formulaire ? [Contact Form 7][wp-plugin-contact].  
Commerce en ligne ? [WooCommerce][wp-plugin-woocommerce].  
Dupliquer un article? [Duplicate][wp-plugin-duplicate].  
SEO ? [Jetpack][wp-plugin-jetpack].  
Trop de spams ? [Aksimet][wp-plugin-aksimet].

Mais avec Jekyll, c'est une autre histoire. J'adore le contrôle que ça me donne sur mes sites :

> Besoin d'un formulaire ? on programme un [`<form>`][html-form].  
Dupliquer un article ? `cp article.md article-copie.md`.

Bien sûr, pour des fonctionnalités plus techniques (par exemple pour [faire une recherche sur le site][jekyll-search] ou [intégrer des Analytics][jekyll-analytics]), un plugin peut toujours aider.

**Mais surtout, les plugins permettent d'ajouter des fonctionnalités sans altérer le code de Jekyll ou d'un thème.**

Et, comme je veux que [mon code source][gitlab-robinmetral-websites] soit facilement réutilisable par d'autres, je dois justement ne pas toucher au code de Jekyll.

Tout ce qu'on devra faire pour réutiliser mon code sera d'installer le même plugin. C'est beaucoup plus facile que de recréer une fonctionnalité DIY en ajoutant un include au thème !

**Moralité : j'ai fait la paix avec les plugins !**

[images-semantiques-jekyll]: {{ site.baseurl }}{% post_url 2018-08-07-des-images-semantiques-avec-jekyll %}
[jekyll-figure]: https://github.com/paulrobertlloyd/jekyll-figure
[gitlab-robinmetral-websites]: https://gitlab.com/robinmetral-websites/
[wp-plugin-contact]: https://wordpress.org/plugins/contact-form-7/
[wp-plugin-woocommerce]: https://wordpress.org/plugins/woocommerce/
[wp-plugin-duplicate]: https://wordpress.org/plugins/duplicate-post/
[wp-plugin-jetpack]: https://wordpress.org/plugins/jetpack/
[wp-plugin-aksimet]: https://wordpress.org/plugins/akismet/
[html-form]: https://developer.mozilla.org/fr/docs/Web/HTML/Element/Form
[jekyll-search]: https://github.com/christian-fei/Simple-Jekyll-Search
[jekyll-analytics]: https://github.com/hendrikschneider/jekyll-analytics
[jekyll-plugins]: https://jekyllrb.com/docs/plugins/
