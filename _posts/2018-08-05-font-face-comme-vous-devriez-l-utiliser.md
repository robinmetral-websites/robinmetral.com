---
layout: post
title: "@font-face comme vous devriez l'utiliser"
date: 2018-08-05 +0700
contenttype: perso
categories: typographie, css, web, tutos
excerpt: "Optimisez votre utilisation de polices personnalisées en CSS avec @font-face"
---
En créant [le site web de Clara][site-clara], qui met l'accent sur la typographie, j'ai eu quelques prises de tête avec les polices de caractères et leur utilisation sur le web.

Ça fait [bientôt dix ans][debuts-webmaster] que je connais `@font-face` mais, sans le savoir, je ne l'utilisais qu'à moitié.

Je me compliquais la tâche !

**Aujourd'hui, je vais vous montrer comment simplifier et améliorer votre utilisation de `@font-face` pour gérer vos polices personnalisées en CSS.**

Mais d'abord, pourquoi utiliser `@font-face` ?


# Intro : les polices et le web

En gros, il y a **trois manières de choisir des polices pour le web**.

1. Opter pour des *[web safe fonts][web-safe-fonts]* : des polices que tout le monde (ou presque) a déjà sur son ordi, comme Arial, Times... ou Comic Sans.
2. Utiliser un service de polices en ligne comme Google Fonts : les polices sont sur les serveurs de Google et sont chargées à chaque visite du site.
3. Définir à la main des polices personnalisées : les polices sont dans un dossier du site, et on les utilise avec la règle CSS `@font-face`.

> Quelle option choisir ?

Pour moi, la décision a vite été prise :

1. <span style="font-family:'Times New Roman'">Le Times New Roman ça va un moment, mais je préfère des polices plus variées et originales.</span>
2. J'aime ne pas dépendre de ressources tierces... encore moins quand les ressources tierces sont chez Google !
3. Donc j'utilise `@font-face`. C'est ce que je vais vous apprendre ci-dessous.


# De quelles variantes avez-vous besoin ?

Une police vient en plusieurs variantes.

Téléchargez-en une, décompressez l'archive, et vous vous retrouverez avec une liste de fichiers qui ressemble à ça:
- MaPolice-Light.ttf
- MaPolice-LightItalic.ttf
- MaPolice-Regular.ttf
- MaPolice-Italic.ttf
- MaPolice-Bold.ttf
- MaPolice-BoldItalic.ttf

Les variantes, c'est tout ce qu'il y a après le tiret : les graisses et le style.

- **les graisses**: une seule police peut venir en 9 graisses différentes, de *MaPolice-Thin* à *MaPolice-Black*
- **le style**: chaque graisse est accompagnée de sa version italique : *MaPolice-ThinItalic* ou encore *MaPolice-BlackItalic*

> Mais est-ce qu'on a vraiment besoin de toutes ces variantes ? Est-ce qu'one pourrait pas simplement utiliser `font-weight` pour la graisse et `font-style` pour le style ? Ou même les éléments HTML `<strong>` et `<em>` qui appliquent `font-weight` et `font-style` (respectivement) par défaut ?

Oui, on pourrait.

... mais si vous avez un oeil pour la typographie (comme [Clara][site-clara]) ou si vous êtes perfectionniste (comme moi), vous ne ferez pas ça.

Voici un exemple de la police Playfair Display Regular :

{% figure caption:"Playfair Display Regular" %}
![Exemple de la police Playfair Display Regular]({{ site.baseurl }}/assets/images/2018-08-04-playfair-regular.png)
{% endfigure %}

Et voici Playfair Display Bold :

{% figure caption:"Playfair Display Bold" %}
![Exemple de la police Playfair Display Bold]({{ site.baseurl }}/assets/images/2018-08-04-playfair-bold.png)
{% endfigure %}

Joli, non ?

Maintenant, comparez ça avec la Regular qu'on met en gras en CSS (avec `font-weight: bold;`) :

{% figure caption:"Playfair Display Regular en gras, c'est moche !" %}
![Exemple de la police Playfair Display Regular mise en gras]({{ site.baseurl }}/assets/images/2018-08-04-playfair-regular-fauxbold.png)
{% endfigure %}

Moralité : une seule variante de police qu'on met en gras ou italique en CSS, ça ne suffit pas.

Choisissez donc toutes les variantes dont vous aurez besoin sur votre site. Moi, j'utilise en général deux graisses par police, donc avec l'italique j'ai quatre variantes par police.


# Optimisez vos polices pour le web

Dernière étape avant de passer au CSS : **convertir chaque variante de police en formats optimisés pour le web**. À l'heure actuelle, [le W3C recommande][w3c-woff2] les formats <abbr title="Web Open Font Format">WOFF</abbr> et WOFF2.

Pour vous faciliter la tâche, je vous conseille le [générateur de webfonts de FontSquirrel][webfont-generator].

On se retrouve donc avec **un fichier .woff et un fichier .woff2 pour chaque variante de chacune de nos polices**.

> Comment est-ce que je les utilise en CSS ?


# @font-face

[La doc][font-face-doc] vous apprendra que `@font-face` s'utilise comme ça :

```css
@font-face {
    font-family: "Ma Police"; /* On nomme la police */
    src: /* On situe la police dans un ou plusieurs formats */
        url("/fonts/MaPolice-Regular.woff2") format("woff2"),
        url("/fonts/MaPolice-Regular.woff") format("woff");
}

p {
    font-family: "Ma Police", sans-serif; /* On déclare la police */
}
```

Ça se complique avec nos variantes de polices.

Non seulement on doit déclarer chaque variante avec `@font-face`, mais on doit aussi s'assurer que la bonne variante soit utilisée dans la bonne situation : je veux *MaPolice-Bold* pour le texte `<strong>` et pour mes titres, *MaPolice-Italic* pour le texte en `em`, etc.


# Comment ne pas utiliser @font-face

Voici comment ne pas utiliser `@font-face`, c'est-à-dire ce que je faisais jusqu'à avant-hier.

Pour l'exemple, je ne déclarerai que *MaPolice-Regular* et *MaPolice-Bold* (pour les variantes italiques, c'est la même chose).

```css
/* MaPolice-Regular */

@font-face {
    font-family: "Ma Police";
    src:
        url("/fonts/MaPolice-Regular.woff2") format("woff2"),
        url("/fonts/MaPolice-Regular.woff") format("woff");
}

body { /* Ce sera ma police par défaut */
    font-family: "Ma Police", sans-serif;
}

/* MaPolice-Bold */

@font-face {
    font-family: "Ma Police Bold";
    src:
        url("/fonts/MaPolice-Bold.woff2") format("woff2"),
        url("/fonts/MaPolice-Bold.woff") format("woff");
}

strong { /* Je veux mes éléments <strong> en gras */
    font-family: "Ma Police Bold", sans-serif;
    font-weight: normal; /* La police est déjà Bold ! */
}

h1, h2, h3, h4, h5, h6 { /* Et mes titres aussi */
    font-family: "Ma Police Bold", sans-serif;
    font-weight: normal; /* La police est déjà Bold ! */
```

Comme vous le voyez, pour `<strong>` et pour chaque titre (`<h1>`, `<h2>` etc.) il fallait remettre `font-weight` à zéro avec `font-weight: normal;` pour éviter que du gras ne soit automatiquement ajouté à une police déjà grasse (on a vu ce que ça donne).

C'est long, c'est compliqué, et c'est très facile de se tromper quelque part.


# Comment utiliser @font-face

Il y a quelques jours, j'ai découvert une technique qui a changé ma vie (ou presque).

Elle n'a rien de secret, et [rien de nouveau][font-face-tip-2010]. La voici :

```css
@font-face {
    font-family: "Ma Police";
    src:
        url("/fonts/MaPolice-Regular.woff2") format("woff2"),
        url("/fonts/MaPolice-Regular.woff") format("woff");
    font-weight: normal; /* Cette police n'est pas en gras */
    font-style: normal; /* Ni en italique */
}

@font-face {
    font-family: "Ma Police"; /* Autre variante mais même police = même nom ! */
    src:
        url("/fonts/MaPolice-Bold.woff2") format("woff2"),
        url("/fonts/MaPolice-Bold.woff") format("woff");
    font-weight: bold; /* Cette variante est en gras ! */
    font-style: normal; /* Mais pas en italique */
}

body {
    font-family: "Ma Police", sans-serif;
}
```

Et c'est tout ! Les éléments qui sont en gras par défaut, comme `<strong>` ou vos titres, s'afficheront automatiquement dans la police *MaPolice-Bold*.

**La clé est de donner un seul et même nom à toutes vos variantes de police, et de déclarer `font-weight` et `font-style` *à l'intérieur* de `@font-face`.**

Vous savez tout ! J'espère que cet article vous aura aidés à simplifier et améliorer votre utilisation de `@font-face`.

[site-clara]: https://clarale.com
[web-safe-fonts]: https://developer.mozilla.org/fr/docs/Learn/CSS/Styling_text/initiation-mise-en-forme-du-texte#Polices_web_s%C3%BBres
[webfont-generator]: https://www.fontsquirrel.com/tools/webfont-generator
[w3c-woff2]: https://www.w3.org/TR/WOFF2/
[font-face-doc]: https://developer.mozilla.org/fr/docs/Web/CSS/@font-face
[playfair-regular]: /assets/images/2018-08-04_playfair-regular.png
[playfair-bold]: /assets/images/2018-08-04_playfair-bold.png
[playfair-regular-fauxbold]: /assets/images/2018-08-04_playfair-regular-fauxbold.png
[font-face-tip-2010]: https://www.456bereastreet.com/archive/201012/font-face_tip_define_font-weight_and_font-style_to_keep_your_css_simple/
[debuts-webmaster]: {{ site.baseurl }}{% post_url 2018-07-30-mes-debuts-de-webmaster %}
