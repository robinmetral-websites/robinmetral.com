---
layout: post
title: "Mes débuts de webmaster"
date: 2018-07-30 +0700
contenttype: perso
categories: webmaster
---
Ça fait aujourd'hui **9 ans, 6 mois et 14 jours que j'apprends à coder**.

C'était le 16 janvier 2009. J'avais 14 ans et j'étais encore au cycle (au collège pour les Français). J'en avais peut-être assez des cours trop scolaires, ou alors je m'étais dit que la nouvelle année était l'occasion d'apprendre quelque chose de nouveau... qui sait.

Ce que je sais, c'est que c'est ce jour-là que j'ai créé un compte sur le Size du Zéro.

Aujourd'hui, le Site du Zéro est devenu [OpenClassrooms][openclassrooms], et c'est la première plateforme d'e-Éducation en Europe. Plus de 1000 cours en ligne, du développement personnel aux systèmes informatiques, vus par plus de 3 millions d'utilisateurs chaque mois.

Mais à l'époque, le Size du Zéro, c'était ça :

{% figure caption:"Le Site du Zéro capturé par la Internet Archive à deux jours de mon inscription !" %}
![Le Site du Zéro le 14 janvier 2009]({{ site.baseurl }}/assets/images/2018-07-30-site-du-zero-archive-2009-01-14.png)
{% endfigure %}

J'ai dévoré le cours XHTML/CSS, puis PHP/MySQL, et bientôt je programmais mon premier site web !

**Je l'ai appelé StatistX.net** (ne cherchez pas, il n'existe plus), et j'y mettais des chiffres et statistiques qui m'impressionnaient, dans le style du livre Guinness des records.

Aujourd'hui, j'ai découvert avec bonheur que **la Internet Archive a capturé StatistX.net avant sa disparition** ! De ses débuts en 2010...

{% figure %}
![StatistX le 15 juillet 2010]({{ site.baseurl }}/assets/images/2018-07-30-statistx-archive-2010-07-15.png)
{% endfigure %}

... à une version plus étoffée deux ans plus tard :

{% figure caption:"Ah, le bon vieux temps des dégradés et des bordures arrondies ! C'est ennuyant le lean design..." %}
![StatistX le 13 février 2012]({{ site.baseurl }}/assets/images/2018-07-30-statistx-archive-2012-02-13.png)
{% endfigure %}

Le projet n'a jamais abouti. StatistX.net n'est (à l'étonnement de tous) pas devenu une référence en matière de statistiques et de records, avec une nombreuse et active communauté de membres passionnés de statistiques.

Mais c'est en le créant que j'ai appris à coder, au rythme de dizaines d'erreurs, d'itérations et de questions sur les forums d'entraide du Site du Zéro.

Aujourd'hui, j'en suis très fier !

[openclassrooms]: https://openclassrooms.com
