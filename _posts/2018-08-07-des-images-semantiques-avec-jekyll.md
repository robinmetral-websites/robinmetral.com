---
layout: post
title: "Des images et légendes sémantiques avec Jekyll"
date: 2018-08-07 +0700
contenttype: perso
categories: images, css, jekyll, tutos, accessibilite, semantique
excerpt: "Markdown ne peut pas générer de HTML sémantiquement correct pour une image avec légende. Voici ma solution."
---
> *11 AOÛT 2018 : j'utilise désormais un plugin plutôt que la solution décrite dans cet article. [Lisez pourquoi][jekyll-figure-plugin].*


J'adore écrire mes articles en [Markdown][markdown-wiki] avec [Jekyll][jekyll-site], mais il y a une chose qui me dérange :

**Markdown ne peut pas générer de HTML sémantiquement correct pour une image avec légende.**

Créé en 2004, Markdown n'a pas été adapté à HTML5 (finalisé en 2014) et à ses nouveaux éléments sémantiques, qui [augmentent l'accessibilité du web][mdn-html-accessibilite].

La façon sémantiquement correcte d'afficher [une image avec légende en HTML5][mdn-figcaption] est la suivante :

```html
<figure>
    <img src="/assets/images/2018-08-07-jekyll-logo.png" alt="Le logo de Jekyll">
    <figcaption>Le logo de Jekyll et son clin d'oeil à Robert Louis Stevenson</figcaption>
</figure>
```

Voici le résultat (avec un peu de CSS) :

{% figure caption:"Le logo de Jekyll et son clin d'oeil à Robert Louis Stevenson" %}
![Le logo de Jekyll]({{ site.baseurl }}/assets/images/2018-08-07-jekyll-logo.png)
{% endfigure %}

# Les images avec légende en Markdown

La difficulté d'afficher une image avec légende en Markdown a déjà été évoqué sur [StackOverflow][stackoverflow].

Pour résumer, les deux grandes solutions proposées sont :

* Mettre l'image et sa légende dans un tableau (horrible, à éliminer tout de suite)
* Mettre la légende en italique sous l'image

La deuxième solution a le mérite d'être facile à utiliser en Markdown :

```markdown
![Le logo de Jekyll](/assets/images/2018-08-07-jekyll-logo.png)
*Le logo de Jekyll et son clin d'oeil à Robert Louis Stevenson*
```

Le HTML généré sera :

```html
<p>
    <img src="/assets/images/2018-08-07-jekyll-logo.png" alt="Le logo de Jekyll">
    <em>Le logo de Jekyll et son clin d'oeil à Robert Louis Stevenson</em>
</p>
```

On peut ensuite appliquer du CSS à la légende avec `img + em { }`.

Facile !

Mais malheureusement, **cette solution n'est pas sémantiquement correcte**.

Que faire ?

# Des légendes sémantiques en Liquid

Ma solution à ce problème a été inspirée par [cet article][image-caption-article].

**L'idée est de créer un extrait de code sémantiquement correct à inclure dans mon Markdown.**

Pour y arriver, j'utilise un [tag personnalisé Liquid][liquid] :

```markdown
{% raw %}
Voici le logo de [Jekyll](https://jekyllrb.com) :

{% include image.html
    src="2018-08-07-jekyll-logo.png" <!-- Nom de mon fichier (situé dans /assets/images) -->
    alt="Le logo de Jekyll" <!-- alt text -->
    caption="Le logo de Jekyll et son clin d'oeil à Robert Louis Stevenson" <!-- Légende -->
%}
{% endraw %}
```

Ce tag appelle mon fichier [`/_includes/image.html`][gitlab-index-html] :

```liquid
{% raw %}
<!-- /_includes/image.html -->
<figure>
    {% if include.url %} <!-- Si il y a une URL, l'image sera cliquable -->
    <a href="{{ include.url }}">
    {% endif %}
    <img
        {% if include.srcabs %} <!-- Source absolue -->
            src="{{ include.srcabs }}"
        {% else %} <!-- Source relative (l'image est dans /assets/images) -->
            src="{{ site.baseurl }}/assets/images/{{ include.src }}"
        {% endif %}
    alt="{{ include.alt }}"> <!-- alt text -->
    {% if include.url %}
    </a>
    {% endif %}
    {% if include.caption %} <!-- Si il y a une légende, on l'affiche -->
        <figcaption>{{ include.caption }}</figcaption>
    {% endif %}
</figure>
{% endraw %}
```

Cet extrait de code affiche une image (par défaut dans le dossier `/assets/images`) avec une légende sémantiquement correcte !

# Autres utilisations

J'utilise le même fichier [`/_includes/image.html`][gitlab-index-html] pour mes différentes utilisations d'images sur Jekyll. Il n'y a que le tag Liquid dans mon Markdown qui change :

## URLs absolues

Pour afficher une image avec son URL absolue (utile pour ne pas devoir avoir toutes ses images sur son propre serveur), je remplace simplement `src` par `srcabs` :

```markdown
{% raw %}
Voici le logo de [Jekyll](https://jekyllrb.com) :

{% include image.html
    srcabs="https://jekyllrb.com/img/logo-2x.png" <!-- Source absolue de l'image -->
    alt="Le logo de Jekyll" <!-- alt text -->
    caption="Le logo de Jekyll et son clin d'oeil à Robert Louis Stevenson" <!-- Légende -->
%}
{% endraw %}
```

## Images cliquables

Pour créer une image cliquable, j'ajoute l'argument `url=" "` :

```markdown
{% raw %}
Voici le logo de [Jekyll](https://jekyllrb.com) :

{% include image.html
    srcabs="https://jekyllrb.com/img/logo-2x.png" <!-- Source absolue de l'image -->
    url="https://jekyllrb.com" <!-- URL de destination -->
    alt="Le logo de Jekyll" <!-- alt text -->
    caption="Le logo de Jekyll et son clin d'oeil à Robert Louis Stevenson" <!-- Légende -->
%}
{% endraw %}
```

## Images sans légendes

Finalement, je peux aussi insérer une `<figure>` sans `<figcaption>`. Seuls `src`/`src-abs` et `alt` sont nécessaires :

```markdown
{% raw %}
Voici le logo de [Jekyll](https://jekyllrb.com) :

{% include image.html
    srcabs="https://jekyllrb.com/img/logo-2x.png" <!-- Source absolue de l'image -->
    alt="Le logo de Jekyll" <!-- alt text -->
%}
{% endraw %}
```

Si vous avez une idée d'amélioration pour ce système, n'hésitez pas à [m'écrire][contact] ou à me faire une pull request !

[jekyll-site]: https://jekyllrb.com
[markdown-wiki]: https://fr.wikipedia.org/wiki/Markdown
[stackoverflow]: https://stackoverflow.com/questions/19331362/using-an-image-caption-in-markdown-jekyll/51682829
[image-caption-article]: https://superdevresources.com/image-caption-jekyll/
[liquid]: https://github.com/Shopify/liquid/wiki/Liquid-for-Designers
[mdn-html-accessibilite]: https://developer.mozilla.org/fr/docs/Apprendre/a11y/HTML
[mdn-figcaption]: https://developer.mozilla.org/fr/docs/Web/HTML/Element/figcaption
[gitlab-index-html]: https://gitlab.com/robinmetral-websites/robinmetral.com/blob/master/_includes/image.html
[contact]: {{ site.baseurl }}{% link _pages/contact.md %}
[jekyll-figure-plugin]: {{ site.baseurl }}{% post_url 2018-08-11-des-images-semantiques-avec-jekyll-2 %}
